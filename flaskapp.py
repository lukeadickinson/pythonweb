from flask import Flask
from flask import request
from flask import redirect
from flask import Response

app = Flask(__name__)

def request_wants_fancy(request):
	best = request.accept_mimetypes.best_match(['application/vnd.byu.cs462.v1+json', 'application/vnd.byu.cs462.v2+json', 'text/html'])
	
	if best == 'application/vnd.byu.cs462.v1+json' and request.accept_mimetypes[best]:
		return "v1"
	elif best == 'application/vnd.byu.cs462.v2+json' and request.accept_mimetypes[best]:
		return "v2"
	else:
		return "none"


@app.route("/", methods=['GET', 'POST'])
def hello():

	fancy = request_wants_fancy(request)

	if(fancy == "v1"):
		jsonToReturn = '{"version": "v1" }'
		return Response(jsonToReturn, mimetype='application/json')
	elif (fancy == "v2"):
		jsonToReturn2 = '{"version": "v2" }'
		return Response(jsonToReturn2, mimetype='application/json')
		
	moo = ""
	moo += "<br/>Request Headers: <br/><br/>"
	
	for e in request.headers:
		moo += str(e) +"<br/>"
		
	moo += "<br/>Query String: <br/><br/>"
	moo += request.query_string +"<br/>"
	
	moo += "<br/>Post Body: <br/><br/>"
	moo += request.get_data() +"<br/>"	
	
	return moo
	

@app.route("/redirect", methods=['GET', 'POST'])
def redirectMethod():

	query = request.query_string
	if query != "":
		if query == "search":
			return redirect("http://www.google.com", code=302)
		elif query == "search2":
			return redirect("http://www.bing.com", code=302)
		elif query == "email":
			return redirect("http://mail.yahoo.com", code=302)
		elif query == "buy":
			return redirect("http://www.amazon.com", code=302)
			
	moo = "Possible redirects <br/><br/>"
	moo += "search -> www.google.com <br/>"
	moo += "search2 -> www.bing.com <br/>"
	moo += "email -> mail.yahoo.com <br/>"
	moo += "buy -> www.amazon.com <br/>"
	return moo
	
	
if __name__ == "__main__":
    app.run()